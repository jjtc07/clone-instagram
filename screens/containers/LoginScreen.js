import React, { Component } from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TextInput, 
  Image, 
  TouchableHighlight,
  // KeyboardAvoidingView, // para subir el teclado se le pasa la propiedad behavior="padding, height o position"
} from 'react-native'

import Container from '../components/Container'
import logoInstagram from '../../images/instagram_logo.png'
import logoFacebook from '../../images/facebook_logo.png'
import { red } from 'ansi-colors';

class LoginScreen extends Component {
  render() {
    return (
      
      <Container style={styles.container}>
        <View style={styles.contentLogin}>
          <Image source={logoInstagram} style={styles.logo} />
          <TextInput
            placeholder='Teléfono, usuario o correo electrónico'
            style={styles.input}
          />
          <TextInput
            placeholder='Contraseña'
            style={styles.input}
          />
          <View style={styles.contentRecuperarPassword}>
            <TouchableHighlight>
              <Text
                style={styles.recuperarPassword}
              >
                ¿Olvidaste tu contraseña?
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.buttonLogin}>
            <TouchableHighlight>
              <Text
                style={{color: '#FFF'}}
              >
                Iniciar sesión
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.lineaDivision}>
            <View style={styles.linea}></View>
            <View style={styles.textLinea}>
              <Text>O</Text>
            </View>
          </View>

          <View style={{marginTop: 40}}>
            <TouchableHighlight>
              <View style={{flexDirection: 'row'}}>
                <Image source={logoFacebook} style={{width: 18, height: 18, marginTop: -2, marginRight: 5}} />
                <Text
                  style={{color: '#07C', fontSize: 13}}
                >
                  Continuar como Jesús Torres
                </Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>

        <View style={styles.footer}>
          <Text>
            ¿No tienes una cuenta?
          </Text>
          <Text style={{...styles.textPrimary, marginLeft: 4, }}>
            Regístrate.
          </Text>
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#FFF',
    justifyContent: 'space-between',
  },
  contentLogin: {
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: 69,
    width: 200,
    marginTop: 70,
    marginBottom: 50,
  },
  input: {
    width: '100%',
    padding: 12,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#ccc',
    backgroundColor: '#FAF9FA',
    marginBottom: 10,
  },
  contentRecuperarPassword: {
    width: '100%',
    alignItems: 'flex-end',
  },
  recuperarPassword: {
    color: '#07C',
    fontSize: 13
  },
  buttonIniciarSesion: {
    backgroundColor: '#5FB0FE',
    color: 'red'
  },
  buttonLogin: {
    marginTop: 40,
    width: '100%',
    padding: 12,
    borderRadius: 5,
    backgroundColor: '#5FB0FE',
    alignItems: 'center',
    color: '#FFF'
  },
  lineaDivision: {
    width: '100%',
    marginTop: 50,
    flexDirection: 'column',
    justifyContent: 'center',
    position: 'relative',
    alignItems: 'center',
  },
  linea:{
    width: '100%',
    height: .7,
    backgroundColor: '#CCC'
  },
  textLinea: {
    position: 'absolute',
    left: 0,
    right: 0,
    alignItems: 'center',
    backgroundColor: '#FFF',
    width: 60,
    marginLeft: 137
  },
  footer: {
    flexDirection: 'row',
    width: '100%',
    bottom: 0,
    padding: 15,
    borderTopWidth: .4,
    borderTopColor: '#ccc',
    justifyContent: 'center',
  },
  textPrimary: {
    color: '#07C',
  }
});

export default LoginScreen;